const { RouteList } = require('@kohanajs/mod-route');

RouteList.add('/', 'controller/Home');
RouteList.add('/pages/:slug', 'controller/Home', 'page');