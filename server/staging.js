const { KohanaJS } = require('kohanajs');

KohanaJS.ENV = 'stg';
const Server = require('./Server');

(async () => {
  const s = new Server(8515);
  await s.setup();
  KohanaJS.configForceUpdate = false;
  KohanaJS.config.classes.cache = true;
  //  require("@kohanajs/mod-route-debug");
  console.log(KohanaJS.ENV, KohanaJS.config);
  await s.listen();
})();
