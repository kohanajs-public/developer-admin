const { KohanaJS } = require('kohanajs');

KohanaJS.ENV = 'uat';
const Server = require('./Server');

(async () => {
  const s = new Server(8514);
  await s.setup();
  //  require("@kohanajs/mod-route-debug");
  console.log(KohanaJS.ENV, KohanaJS.config);
  await s.listen();
})();
