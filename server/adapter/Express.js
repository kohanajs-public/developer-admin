const express = require('express');
const { KohanaJS } = require('kohanajs');
const { RouteList, RouterAdapterExpress: RouteAdapter } = require('@kohanajs/mod-route');

class ServerAdapterExpress {
  static async setup() {
    const app = express();

    if (KohanaJS.config.cookie) {
      const cookieParser = require('cookie-parser');
      app.use(cookieParser(KohanaJS.config.cookie.salt, KohanaJS.config.cookie.options));
    }

    RouteList.createRoute(app, RouteAdapter);

    return app;
  }
}

module.exports = ServerAdapterExpress;
