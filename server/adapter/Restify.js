const restify = require('restify');
const { KohanaJS } = require('kohanajs');
const { RouteList, RouterAdapterRestify: RouteAdapter } = require('@kohanajs/mod-route');

class ServerAdapterFastify {
  static async setup() {
    const app = restify.createServer();

    if (KohanaJS.config.cookie) {
      const cookieParser = require('restify-cookies');
      app.use(cookieParser.parse);
    }

    RouteList.createRoute(app, RouteAdapter);

    return app;
  }
}

module.exports = ServerAdapterFastify;
