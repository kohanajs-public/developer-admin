const { KohanaJS } = require('kohanajs');

KohanaJS.ENV = 'dev';
const Server = require('./Server');

(async () => {
  const s = new Server(8519);
  await s.setup();
  require('@kohanajs/mod-route-debug');
  console.log(KohanaJS.ENV, KohanaJS.config);
  await s.listen();
})();
